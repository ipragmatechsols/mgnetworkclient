#
# Be sure to run `pod lib lint MGNetworkClient.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'MGNetworkClient'
s.version          = '0.2.1'
s.summary          = 'MGNetworkClient is to handle all the network related and Models etc'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
TODO: MGNetworkClient is to handle all the network related and Models etc(Swift 3.0)
DESC

s.homepage         = 'https://ipragmatechsols@bitbucket.org/ipragmatechsols/mgnetworkclient.git'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'iPragmatech' => 'info@ipragmatech.com' }
s.source           = { :git => 'https://ipragmatechsols@bitbucket.org/ipragmatechsols/mgnetworkclient.git', :tag => s.version.to_s,:commit => "793ea21"}
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '9.0'

s.source_files = 'MGNetworkClient/Classes/**/*'

 s.resource_bundles = {
   'MGNetworkClient' => ['MGNetworkClient/Assets/*.png']
 }

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'IPServerUtility'
  s.dependency 'LNRSimpleNotifications'
# s.dependency 'ObjectMapper', '~> 1.3'

end
