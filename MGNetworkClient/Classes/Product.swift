//
//  Product.swift
//  Pods
//
//  Created by Bunty on 02/07/16.
//
//

import UIKit
import SwiftyJSON
public class Product: NSObject {
    public  var id: Int?
    public  var sku: String?
    public  var product_id: String?
    public  var qauntity: Int?
    public  var prodDescription: String?
    public  var base_price: String?
    public  var name: String?
    public  var image: String?
    public  var size: String?
    public  var color: String?
    public  var position: Int?
    public var category_id: String?
    public var price: Int?
    public var amount: String?
    public var status: Int?
    public var visibility: Int?
    public var type_id: String?
    public var created_at: String?
    public var updated_at: String?
    
    public var weight: Int?
    public var attribute_set_id: Int?
    public var custom_attributes:[Attribute]?
    public var extension_attributes:[ExtAttribute]?
    public var medias:[Media]?
    public var options:[Option]?
    public var is_in_stock:Bool?
    //public var tier_prices: String?
    //public var options: String?
    // public var product_links: []?
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.sku  = map?["sku"].string
        self.product_id  = map?["product_id"].string
        self.prodDescription  = map?["description"].string
        self.qauntity  = map?["qty"].int
        self.base_price  = map?["base_price"].string
        self.size  = map?["size"].string
        self.color  = map?["color"].string
        self.image  = map?["image"].string
        self.position    = map?["position"].int
        self.category_id        =  map?["category_id"].string
        self.id        =  map?["id"].int
        self.name        =  map?["name"].string
        self.attribute_set_id        =  map?["attribute_set_id"].int
        self.price        =  map?["price"].int
        self.price        =  map?["price"].int
        self.status        =  map?["status"].int
        self.visibility        =  map?["visibility"].int
        self.type_id        =  map?["type_id"].string
        self.weight        =  map?["weight"].int
        self.created_at        =  map?["created_at"].string
        self.updated_at        =  map?["updated_at"].string
        
        if self.amount == nil  {
            let p:String?        =  map?["price"].string
            if p != nil
            {
                let amount = Float(p!)
                let  amountText =   String(format: "%.2f", amount!)
                self.amount =  amountText;
                
                
            }
        }
        
        if self.image != nil
        {
            if self.image!.range(of:"http") != nil {
                self.image =  MGNetworkClient.sharedClient.imageBaseUrl + MGNetworkClient.sharedClient.imageMiddleUrl + self.image!;
            }
        }
        let customAttributs = map?["custom_attributes"];
        let rawExtension_attributes = map?["extension_attributes"];
        let configurable_product_options = rawExtension_attributes?["configurable_product_options"];
        
        var media_gallery_entries = map?["media_gallery_entries"];
        
        
        if media_gallery_entries == nil {
            media_gallery_entries = map?["media_gallery"]["images"]
        }

        
        
        
        
        if let c = configurable_product_options {
            var customAttributesMdlAr = [ExtAttribute]();
            
            for (key,subJson):(String, JSON) in c {
                let attr = ExtAttribute.init(map: subJson);
                customAttributesMdlAr.append(attr);
            }
            self.extension_attributes = customAttributesMdlAr;
        }
        
        
        if let c = customAttributs {
            var customAttributesMdlAr = [Attribute]();
            
            for (key,subJson):(String, JSON) in c {
                let attr = Attribute.init(map: subJson);
                customAttributesMdlAr.append(attr);
            }
            self.custom_attributes = customAttributesMdlAr;
        }
        
        if let c = media_gallery_entries {
            var customAttributesMdlAr = [Media]();
            
            for (key,subJson):(String, JSON) in c {
                let attr = Media.init(map: subJson);
                customAttributesMdlAr.append(attr);
            }
            
            if self.custom_attributes != nil {
                let imgURL:String
                for attr:Attribute in self.custom_attributes! {
                    if attr.attribute_code == "image" {
                        imgURL = attr.value!;
                        var firstOBjINdex = 0;
                        var  i = 0;
                        for meAtt in customAttributesMdlAr {
                            if meAtt.file == imgURL {
                                firstOBjINdex = i;
                                break
                            }
                            i += 1;
                        }
                        if customAttributesMdlAr.count > 0  && firstOBjINdex != 0 {
                            swap(&customAttributesMdlAr[0], &customAttributesMdlAr[firstOBjINdex])
                        }
                        
                        break;
                    }
                }
            }
            self.medias = customAttributesMdlAr;
        }
        
        let opts  = map?["options"]
        
        var valArr = [Option]();
        if opts != nil {
            for (key,subJson):(String, JSON) in  opts! {
                let value = Option.init(map: subJson);
                // if value.optionsValues?.count>0 {
                valArr.append(value);
                //}
                
                
            }
            
            self.options = valArr;
        }

    }
}

public class Option: NSObject {
    
    public  var product_sku: String?
    public  var option_id: Int?
    public  var title: String?
    public  var type: String?
    public  var sort_order: Int?
    public  var optionsValues = [OptionValue]()
    public  var selectedValue: String?
    
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.product_sku  = map?["product_sku"].string
        self.option_id  = map?["option_id"].int
        self.title  = map?["title"].string
        self.type  = map?["type"].string
        self.sort_order  = map?["sort_order"].int
        self.selectedValue = nil;
        let opvalues  = map?["values"]
        
        var valArr = [OptionValue]();
        if opvalues != nil {
            for (key,subJson):(String, JSON) in  opvalues! {
                let value = OptionValue.init(map: subJson);
                
                valArr.append(value);
                
            }
            
            self.optionsValues = valArr;
        }
        
        
    }
}


public class OptionValue: NSObject {
    
    public  var price: String?
    public  var option_type_id: Int?
    public  var title: String?
    public  var price_type: String?
    public  var sku: String?
    public  var sort_order: Int?
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.price  = map?["price"].string
        self.option_type_id  = map?["option_type_id"].int
        self.title  = map?["title"].string
        self.price_type  = map?["price_type"].string
        self.sort_order  = map?["sort_order"].int
        self.sku  = map?["sku"].string
        
    }
}



public class Attribute: NSObject {
    
    public  var attribute_code: String?
    public  var value: String?
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.attribute_code  = map?["attribute_code"].string
        self.value    = map?["value"].string
        if self.attribute_code == nil {
            self.attribute_code  = map?["option_id"].string
        }
        
        if self.value == nil {
            self.value  = map?["option_value"].string
        }
        
        if self.attribute_code != nil {
            
            
            if (self.attribute_code!.range(of:"http") != nil || self.attribute_code!.range(of:"http") != nil) {
                if !(self.value!.range(of:"http") != nil) {
                    self.value =  MGNetworkClient.sharedClient.imageBaseUrl + MGNetworkClient.sharedClient.imageMiddleUrl + self.value!;
                }
            }
        }
        
    }
}
public class ExtAttribute: NSObject {
    
    public  var id: Int?
    public  var attribute_id: String?
    public  var label: String?
    public  var position: Int?
    public  var values: [Int]?
    public  var product_id: Int?
    // public  var value: String?
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.id  = map?["id"].int
        self.attribute_id  = map?["attribute_id"].string
        self.label  = map?["label"].string
        self.position  = map?["position"].int
        let rawvalues  = map?["values"]
        self.product_id  = map?["product_id"].int
        var valArr = [Int]();
        if rawvalues != nil {
            for (key,subJson):(String, JSON) in  rawvalues! {
                let value = subJson["value_index"].int
                if value != nil {
                    valArr.append(value!);
                }
                
            }
            
            self.values = valArr;
        }
        
        
        
    }
}

public class Media : NSObject{
    
    
    public var id:Int?
    public var media_type:String?
    public var label:String?
    public var position:Int?
    public var disabled:Bool?
    // public var types:Int?
    public var file:String?
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.id  = map?["id"].int
        self.media_type  = map?["media_type"].string
        self.label  = map?["label"].string
        self.position  = map?["position"].int
        let disabled  = map?["disabled"].bool
        self.file  = map?["file"].string
        if self.file != nil
        {
            if !(self.file!.range(of: "http") != nil) {
                self.file =  MGNetworkClient.sharedClient.imageBaseUrl + MGNetworkClient.sharedClient.imageMiddleUrl + self.file!;
            }
        }
    }
    
}

public class CartItem:Product
{
    public var item_id:Int?
    public var qty:Int?
    public var quote_id:String?
    public var product_type:String?
    public var product_options:[Attribute]?
    public var product:Product?
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        
        self.init()
        let p = Product.init(map: map);
        self.product = p;
        self.item_id  = map?["item_id"].int
        self.qty  = map?["qty"].int
        self.product_type  = map?["product_type"].string
        self.quote_id  = map?["quote_id"].string
        self.price  = map?["price"].int
        
        if  self.price == 0 ||  self.price == nil {
            let productDict = MGNetworkClient.sharedClient.productDict();
            for (key,value) in productDict {
                if key == self.product?.sku
                {
                    self.price = Int(value)
                }
            }
            
        }
        let product_option = map?["product_option"]
        let extension_attributes = product_option?["extension_attributes"]
        let custom_options = extension_attributes?["custom_options"]
        if let c = custom_options {
            var customAttributesMdlAr = [Attribute]();
            
            for (key,subJson):(String, JSON) in c {
                let attr = Attribute.init(map: subJson);
                customAttributesMdlAr.append(attr);
            }
            self.product_options = customAttributesMdlAr;
        }
        
        
    }
    
    
    public convenience init(product:Product!)
    {
        self.init()
        self.item_id = product.id;
        self.product_type = product.type_id;
        self.sku  = product.sku;
        self.price = product.price;
        
    }
    
    public func getParamsFromProduct(cart:CartItem! , custom_options:[[String:String]]) -> AnyObject
    {
        var cartDict = [String:AnyObject?]()
        var itemDict = NSMutableDictionary()
        // itemDict.setValue(cart?.item_id, forKey: "item_id");
        // itemDict["item_id"] = cart?.item_id;
        
        
        itemDict.setValue(cart?.sku, forKey: "sku");
        
        itemDict.setValue(cart?.qty, forKey: "qty");
        
        itemDict.setValue(cart?.price, forKey: "price");
        
        itemDict.setValue(cart?.type_id, forKey: "product_type");
        
        itemDict.setValue(cart?.quote_id, forKey: "quote_id");
        
        var product_option = NSMutableDictionary()
        var extension_attributes = NSMutableDictionary()
        extension_attributes.setValue(custom_options, forKey: "custom_options");
        product_option.setValue(extension_attributes, forKey: "extension_attributes");
        
        itemDict["product_option"] = product_option;
        return itemDict as AnyObject;
        
        
    }
    public func getParamsFromCartItem(cart:CartItem!) -> AnyObject
    {
        var cartDict = [String:AnyObject?]()
        var itemDict = NSMutableDictionary()
        itemDict.setValue(cart?.item_id, forKey: "item_id");
        itemDict["item_id"] = cart?.item_id;
        
        
        itemDict.setValue(cart?.sku, forKey: "sku");
        
        itemDict.setValue(cart?.qty, forKey: "qty");
        
        itemDict.setValue(cart?.price, forKey: "price");
        
        itemDict.setValue(cart?.type_id, forKey: "product_type");
        
        itemDict.setValue(cart?.quote_id, forKey: "quote_id");
        
        
        var attrArr = [[String:AnyObject]]()
        for at:Attribute in cart.product_options! {
            var tempDIct =  [String:AnyObject]();
            tempDIct["option_id"] = at.attribute_code as AnyObject?;
            tempDIct["option_value"] = at.value as AnyObject?;
            attrArr.append(tempDIct);
        }
        
        
        var product_option = NSMutableDictionary()
        var extension_attributes = NSMutableDictionary()
        extension_attributes.setValue(attrArr, forKey: "custom_options");
        product_option.setValue(extension_attributes, forKey: "extension_attributes");
        
        itemDict["product_option"] = product_option;
        return itemDict as AnyObject;
        
        
    }
}


public class WishListItem:Product
{
    public var wishlist_item_id:String?
    public var wishlist_id:String?
    public var quote_id:String?
    public var product_type:String?
    public var product_options:[Attribute]?
    public var product:Product?
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        
        self.init()
        let p = Product.init(map: map!["product"]);
        self.product = p;
        self.wishlist_item_id  = map?["wishlist_item_id"].string
        self.wishlist_id  = map?["wishlist_id"].string
        self.product_type  = map?["product_type"].string
        self.quote_id  = map?["quote_id"].string
        self.price  = map?["price"].int
        
    }
}

public class Payment:NSObject
{
    public var method:String?
    
    
    public func getParamsWithPaymentMethod(method:String!)  -> [String:AnyObject]?
    {
       /* var finalparams =  [String:AnyObject]();
        let params =  NSMutableDictionary();
        params["method"] = method;
        finalparams["paymentMethod"] = params
        return finalparams;*/
        var finalparams =  [String:AnyObject]();
        let params =  NSMutableDictionary();
        params["method"] = method;
        
        
        finalparams["paymentMethod"] = params
        
        
        let shippingParams =  NSMutableDictionary();
        shippingParams["additionalProperties"] = [];
        shippingParams["carrier_code"] = "flatrate";
        shippingParams["method_code"] = "flatrate";
        
        
        finalparams["shippingMethod"] = shippingParams
        
        
        
        return finalparams;
    }
    
}


public class Order:NSObject
{
    public var order_id:String?
    public var status:String?
    public var amount:String?
    
    public var order_date:String?
    
    public var shipping_charge:String?
    
    public var tax:String?
    
    public var couponcode:String?
    
    public var qty:String?
    public var products:[Product]?
    
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.order_id  = map?["order_id"].string
        self.status    = map?["status"].string
        self.amount        =  map?["amount"].string
        self.order_date        =  map?["order_date"].string
        self.shipping_charge        =  map?["shipping_charge"].string
        self.tax        =  map?["tax"].string
        self.couponcode        =  map?["couponcode"].string
        self.status        =  map?["status"].string
        self.qty        =  map?["qty"].string
        // self.products        =  map?["type_id"].string
        let products = map?["products"]
        if let p = products
        {
            var customMdlAr = [Product]();
            for (key,subJson):(String, JSON) in p {
                let attr = Product.init(map: subJson);
                customMdlAr.append(attr);
            }
            self.products = customMdlAr;
        }
        
        
    }
    
    
    
    
    
    
}


public class ShippingInfo:NSObject
{
    public var payment_methods:[ShippingSegment]?
    public var total_segments:[ShippingSegment]?
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        
        let products = map?["payment_methods"]
        if let p = products
        {
            var customMdlAr = [ShippingSegment]();
            for (key,subJson):(String, JSON) in p {
                let attr = ShippingSegment.init(map: subJson);
                customMdlAr.append(attr);
            }
            self.payment_methods = customMdlAr;
        }
        let total = map?["totals"]
        let segments = total?["total_segments"]
        if let p = segments
        {
            var customMdlAr = [ShippingSegment]();
            for (key,subJson):(String, JSON) in p {
                let attr = ShippingSegment.init(map: subJson);
                customMdlAr.append(attr);
            }
            self.total_segments = customMdlAr;
        }
        
    }
    
    
    
    
    
    
}

public class ShippingSegment:NSObject
{
    public var title:String?
    public var value:Float?
    public var code:String?
    
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.title  = map?["title"].string
        self.value  = map?["value"].float
        self.code  = map?["code"].string
        
        
        
    }
    
    
    
    
    
    
}

public class Review:NSObject
{
    public var title:String?
    public var detail:String?
    public var review_id:String?
    public var entity_id:String?
    public var nickname:String?
    public var customer_id:String?
    public var detail_id:String?
    public var entity_code:String?
    public var created_at:String?
    
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON?) {
        self.init()
        self.title  = map?["title"].string
        self.detail  = map?["detail"].string
        self.review_id  = map?["review_id"].string
        
        self.entity_id  = map?["entity_id"].string
        self.nickname  = map?["nickname"].string
        self.customer_id  = map?["customer_id"].string
        self.detail_id  = map?["detail_id"].string
        self.entity_code  = map?["entity_code"].string
        self.created_at  = map?["created_at"].string
        
        
        
    }
    
    
    
    
    
    
}
