//
//  ServerUtility.swift
//  ServerUtility
//
//  Created by vikaskumar on 6/10/16.
//  Copyright © 2016 vikaskumar. All rights reserved.
//  Comment
//Updated to swift 2.3
//ADDED PUT COMPATIBILITY


import UIKit
import Alamofire
import SwiftyJSON


public enum RequesType  {
    case GET
    case POST
    case DELETE
    case PUT
}

public class ServerUtility {
    
    // typealias  ResponseHandler = ((NSError? , [String:AnyObject]?) -> Void)?
    public  typealias ResponseHandler = (NSError?, JSON?)->()?
    public static let sharedInstance = ServerUtility()
    public var baseUrl:String?
    public var authToken:String?
    private init() {}
    
    public func setConfiguration(baseUrl:String) {
        self.baseUrl = baseUrl;
    }
    
    public func makeRequest(ofType type:RequesType,  onPath:String, withParams:[String:AnyObject]?, responseHandler:@escaping ResponseHandler )  {
        
        assert(self.baseUrl != nil );//SetConfigurations First;
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let fullUrlStr  = self.baseUrl! + onPath;
        let fullUrl = URL(string: fullUrlStr)
       
        
        //  let headers = [""]
        var headers = [String:String]();
        if self.authToken != nil {
            headers = [
                "Authorization": "Bearer " + self.authToken!,
                "Content-Type": "application/json"
            ]
        }
        else
        {
            headers = [
                "Content-Type": "application/json"
            ]
        }
        
        
        
        if type == .GET {
            
            
            
            Alamofire.request(fullUrl!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let value = response.result.value {
                    let json = JSON(value)
                    responseHandler(nil,json);
                    
                }
                else
                {
                    let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                    responseHandler(error,nil);
                }
                
            }
            
            
            
           /* Alamofire.request(.GET, fullUrl,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        responseHandler(nil,json);
                        
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                case .Failure(let error):
                    responseHandler(error,nil);
                }
            }*/
            
            
        }
        else if type == .POST
        {
            
            
            
            
            Alamofire.request(fullUrl!, method: .post, parameters: withParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let value = response.result.value {
                    let json = JSON(value)
                    responseHandler(nil,json);
                    
                }
                else
                {
                    let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                    responseHandler(error,nil);
                }
            }
            
            
            
           /*
            Alamofire.request(.POST, fullUrl,parameters: withParams,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print(value)
                        responseHandler(nil,json);
                        
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                case .Failure(let error):
                    responseHandler(error,nil);
                }
            }*/
            
        }
        else if type == .DELETE
        {
            
            
            Alamofire.request(fullUrl!, method: .delete, parameters: withParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let value = response.result.value {
                    let json = JSON(value)
                    responseHandler(nil,json);
                    
                }
                else
                {
                    let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                    responseHandler(error,nil);
                }
            }
            
            /*
            Alamofire.request(.DELETE, fullUrl,parameters: withParams,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print(value)
                        responseHandler(nil,json);
                        
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                case .Failure(let error):
                    responseHandler(error,nil);
                }
            }*/
        }
        
        else if type == .PUT
        {
            
            Alamofire.request(fullUrl!, method: .put, parameters: withParams, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let value = response.result.value {
                    let json = JSON(value)
                    responseHandler(nil,json);
                    
                }
                else
                {
                    let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                    responseHandler(error,nil);
                }
            }
            
            
           /* Alamofire.request(.PUT, fullUrl,parameters: withParams,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print(value)
                        responseHandler(nil,json);
                        
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                case .Failure(let error):
                    responseHandler(error,nil);
                }
            }*/
        }
        
        
    }
    
    
    
    
    
}
