//
//  Banner.swift
//  Pods
//
//  Created by Bunty on 02/07/16.
//
//

import UIKit
import SwiftyJSON


public class Banner: NSObject {
    
    public  var id: String?
    public  var name: String?
    public var imageUrl: String?
    public var subcategory_children:[String]?
    public var sku:String?
    
    
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON?) {
        self.init()
        self.id  = map?["id"].string
        self.name    = map?["name"].string
        self.imageUrl        =  map?["image"].string
        self.sku = map?["sku"].string;
        if self.sku == nil {
            self.sku = map?["link_product_sku"].string;
            
        }
        if(self.name == nil)
        {
          self.name    = map?["title"].string
        }
        if(self.name == nil)
        {
            self.name    = map?["category_name"].string
        }
        if(self.id == nil)
        {
            self.id    = map?["category_id"].string
        }
        if(self.imageUrl == nil)
        {
            self.imageUrl    = map?["category_image"].string
        }
        self.subcategory_children = map?["subcategory_children"].arrayObject as? [String]
        if self.imageUrl != nil  {
            if self.imageUrl?.range(of:"http") != nil{
                
                if  map?["sku"].string != nil {
                    self.imageUrl =  MGNetworkClient.sharedClient.imageBaseUrl + MGNetworkClient.sharedClient.imageMiddleUrl + self.imageUrl!;
                }
                else
                {
                    self.imageUrl =  MGNetworkClient.sharedClient.imageBaseUrl + "/media/" + self.imageUrl!;
                }
            }
        }
        
        /*
        if self.imageUrl != nil  {
            
            if self.imageUrl?.range(of:"http") != nil{
            
            //if (!(self.imageUrl!.containsString("http"))) {
             self.imageUrl =  MGNetworkClient.sharedClient.imageBaseUrl + "/pub/media/catalog/product" + self.imageUrl!;
            }
        }*/
       
    }
  
}
