# IPServerUtility

[![CI Status](http://img.shields.io/travis/ipragmatech/IPServerUtility.svg?style=flat)](https://travis-ci.org/ipragmatech/IPServerUtility)
[![Version](https://img.shields.io/cocoapods/v/IPServerUtility.svg?style=flat)](http://cocoapods.org/pods/IPServerUtility)
[![License](https://img.shields.io/cocoapods/l/IPServerUtility.svg?style=flat)](http://cocoapods.org/pods/IPServerUtility)
[![Platform](https://img.shields.io/cocoapods/p/IPServerUtility.svg?style=flat)](http://cocoapods.org/pods/IPServerUtility)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IPServerUtility is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "IPServerUtility"
```

## Author

ipragmatech, info@ipragmatech.com

## License

IPServerUtility is available under the MIT license. See the LICENSE file for more info.
